EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 2 6
Title "uSDX Pi Radio Audio Section"
Date "2020-09-22"
Rev "0.1"
Comp "Offline Systems LLC"
Comment1 "Andrew Roy Jackman"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 2020-09-22_16-34-13:PCM1804DBR U?
U 1 1 5F91C140
P 1900 3100
AR Path="/5F91C140" Ref="U?"  Part="1" 
AR Path="/5F911003/5F91C140" Ref="U201"  Part="1" 
F 0 "U201" H 3400 3487 60  0000 C CNN
F 1 "PCM1804DBR" H 3400 3381 60  0000 C CNN
F 2 "Package_SO:SSOP-28_5.3x10.2mm_P0.65mm" H 3400 3340 60  0001 C CNN
F 3 "" H 1900 3100 60  0000 C CNN
	1    1900 3100
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR?
U 1 1 5F91C146
P 5650 3800
AR Path="/5F91C146" Ref="#PWR?"  Part="1" 
AR Path="/5F911003/5F91C146" Ref="#PWR0204"  Part="1" 
F 0 "#PWR0204" H 5650 3550 50  0001 C CNN
F 1 "Earth" H 5650 3650 50  0001 C CNN
F 2 "" H 5650 3800 50  0001 C CNN
F 3 "~" H 5650 3800 50  0001 C CNN
	1    5650 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F91C14C
P 5350 3100
AR Path="/5F91C14C" Ref="C?"  Part="1" 
AR Path="/5F911003/5F91C14C" Ref="C204"  Part="1" 
F 0 "C204" V 5300 3250 50  0000 C CNN
F 1 "1u" V 5300 3000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5388 2950 50  0001 C CNN
F 3 "~" H 5350 3100 50  0001 C CNN
	1    5350 3100
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5F91C152
P 5350 3300
AR Path="/5F91C152" Ref="C?"  Part="1" 
AR Path="/5F911003/5F91C152" Ref="C205"  Part="1" 
F 0 "C205" V 5300 3450 50  0000 C CNN
F 1 "1u" V 5300 3200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5388 3150 50  0001 C CNN
F 3 "~" H 5350 3300 50  0001 C CNN
	1    5350 3300
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5F91C158
P 5350 3700
AR Path="/5F91C158" Ref="C?"  Part="1" 
AR Path="/5F911003/5F91C158" Ref="C206"  Part="1" 
F 0 "C206" V 5300 3850 50  0000 C CNN
F 1 "1u" V 5300 3600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5388 3550 50  0001 C CNN
F 3 "~" H 5350 3700 50  0001 C CNN
	1    5350 3700
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5F91C15E
P 1200 3100
AR Path="/5F91C15E" Ref="C?"  Part="1" 
AR Path="/5F911003/5F91C15E" Ref="C201"  Part="1" 
F 0 "C201" V 1150 2950 50  0000 C CNN
F 1 "1u" V 1150 3200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1238 2950 50  0001 C CNN
F 3 "~" H 1200 3100 50  0001 C CNN
	1    1200 3100
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5F91C164
P 1200 4400
AR Path="/5F91C164" Ref="C?"  Part="1" 
AR Path="/5F911003/5F91C164" Ref="C203"  Part="1" 
F 0 "C203" V 1150 4250 50  0000 C CNN
F 1 "1u" V 1150 4500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1238 4250 50  0001 C CNN
F 3 "~" H 1200 4400 50  0001 C CNN
	1    1200 4400
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5F91C16A
P 1200 3300
AR Path="/5F91C16A" Ref="C?"  Part="1" 
AR Path="/5F911003/5F91C16A" Ref="C202"  Part="1" 
F 0 "C202" V 1150 3150 50  0000 C CNN
F 1 "1u" V 1150 3400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1238 3150 50  0001 C CNN
F 3 "~" H 1200 3300 50  0001 C CNN
	1    1200 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	5650 3100 5650 3200
Wire Wire Line
	1350 3300 1900 3300
$Comp
L power:Earth #PWR?
U 1 1 5F91C174
P 750 4550
AR Path="/5F91C174" Ref="#PWR?"  Part="1" 
AR Path="/5F911003/5F91C174" Ref="#PWR0201"  Part="1" 
F 0 "#PWR0201" H 750 4300 50  0001 C CNN
F 1 "Earth" H 750 4400 50  0001 C CNN
F 2 "" H 750 4550 50  0001 C CNN
F 3 "~" H 750 4550 50  0001 C CNN
	1    750  4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  3300 750  4300
Wire Wire Line
	750  3300 1050 3300
Wire Wire Line
	750  3100 750  3200
Connection ~ 750  3300
Connection ~ 5650 3600
Connection ~ 750  4300
Wire Wire Line
	5500 3100 5650 3100
Wire Wire Line
	750  4300 750  4400
Wire Wire Line
	1050 4400 750  4400
Connection ~ 750  4400
Wire Wire Line
	750  4400 750  4550
Wire Wire Line
	1050 3100 750  3100
$Comp
L power:+3V3 #PWR?
U 1 1 5F91C18B
P 1450 3050
AR Path="/5F91C18B" Ref="#PWR?"  Part="1" 
AR Path="/5F911003/5F91C18B" Ref="#PWR0202"  Part="1" 
F 0 "#PWR0202" H 1450 2900 50  0001 C CNN
F 1 "+3V3" H 1465 3223 50  0000 C CNN
F 2 "" H 1450 3050 50  0001 C CNN
F 3 "" H 1450 3050 50  0001 C CNN
	1    1450 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3600 5650 3700
Wire Wire Line
	5500 3700 5650 3700
Connection ~ 5650 3700
Wire Wire Line
	5650 3700 5650 3800
Wire Wire Line
	1450 3050 1450 3600
Connection ~ 5650 3200
Connection ~ 750  3200
Wire Wire Line
	750  3200 750  3300
NoConn ~ 4900 4100
NoConn ~ 4900 4000
NoConn ~ 4900 3900
NoConn ~ 4900 3800
$Comp
L power:+5V #PWR?
U 1 1 5F91C1A3
P 5150 3000
AR Path="/5F91C1A3" Ref="#PWR?"  Part="1" 
AR Path="/5F911003/5F91C1A3" Ref="#PWR0203"  Part="1" 
F 0 "#PWR0203" H 5150 2850 50  0001 C CNN
F 1 "+5V" H 5165 3173 50  0000 C CNN
F 2 "" H 5150 3000 50  0001 C CNN
F 3 "" H 5150 3000 50  0001 C CNN
	1    5150 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 3700 4900 3700
Wire Wire Line
	5200 3700 5150 3700
Connection ~ 5150 3700
Wire Wire Line
	5150 3700 5150 3000
Text GLabel 4900 4400 2    50   Input ~ 0
I2S_SD
Text GLabel 4900 4200 2    50   Input ~ 0
I2S_WS
Text GLabel 4900 4300 2    50   Input ~ 0
I2S_SCK
$Comp
L Device:R R?
U 1 1 5F936287
P 6900 3000
AR Path="/5F936287" Ref="R?"  Part="1" 
AR Path="/5F911003/5F936287" Ref="R208"  Part="1" 
F 0 "R208" V 6693 3000 50  0000 C CNN
F 1 "270" V 6784 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6830 3000 50  0001 C CNN
F 3 "~" H 6900 3000 50  0001 C CNN
	1    6900 3000
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5F93628D
P 7150 3150
AR Path="/5F93628D" Ref="C?"  Part="1" 
AR Path="/5F911003/5F93628D" Ref="C207"  Part="1" 
F 0 "C207" H 7265 3196 50  0000 L CNN
F 1 "33n" H 7265 3105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7188 3000 50  0001 C CNN
F 3 "~" H 7150 3150 50  0001 C CNN
	1    7150 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F936293
P 7550 3150
AR Path="/5F936293" Ref="R?"  Part="1" 
AR Path="/5F911003/5F936293" Ref="R210"  Part="1" 
F 0 "R210" H 7620 3196 50  0000 L CNN
F 1 "150" H 7620 3105 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7480 3150 50  0001 C CNN
F 3 "~" H 7550 3150 50  0001 C CNN
	1    7550 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F936299
P 7900 3000
AR Path="/5F936299" Ref="C?"  Part="1" 
AR Path="/5F911003/5F936299" Ref="C209"  Part="1" 
F 0 "C209" V 7648 3000 50  0000 C CNN
F 1 "10u" V 7739 3000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7938 2850 50  0001 C CNN
F 3 "~" H 7900 3000 50  0001 C CNN
	1    7900 3000
	0    1    1    0   
$EndComp
Wire Wire Line
	7550 3000 7150 3000
Connection ~ 7150 3000
$Comp
L power:Earth #PWR?
U 1 1 5F9362A1
P 7550 3300
AR Path="/5F9362A1" Ref="#PWR?"  Part="1" 
AR Path="/5F911003/5F9362A1" Ref="#PWR0205"  Part="1" 
F 0 "#PWR0205" H 7550 3050 50  0001 C CNN
F 1 "Earth" H 7550 3150 50  0001 C CNN
F 2 "" H 7550 3300 50  0001 C CNN
F 3 "~" H 7550 3300 50  0001 C CNN
	1    7550 3300
	1    0    0    -1  
$EndComp
Connection ~ 7550 3300
Wire Wire Line
	7150 3300 7550 3300
Wire Wire Line
	7050 3000 7150 3000
Wire Wire Line
	7750 3000 7550 3000
Connection ~ 7550 3000
$Comp
L Device:R R?
U 1 1 5F9362AC
P 6900 3550
AR Path="/5F9362AC" Ref="R?"  Part="1" 
AR Path="/5F911003/5F9362AC" Ref="R209"  Part="1" 
F 0 "R209" V 6693 3550 50  0000 C CNN
F 1 "270" V 6784 3550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6830 3550 50  0001 C CNN
F 3 "~" H 6900 3550 50  0001 C CNN
	1    6900 3550
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5F9362B2
P 7150 3700
AR Path="/5F9362B2" Ref="C?"  Part="1" 
AR Path="/5F911003/5F9362B2" Ref="C208"  Part="1" 
F 0 "C208" H 7265 3746 50  0000 L CNN
F 1 "33n" H 7265 3655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7188 3550 50  0001 C CNN
F 3 "~" H 7150 3700 50  0001 C CNN
	1    7150 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F9362B8
P 7550 3700
AR Path="/5F9362B8" Ref="R?"  Part="1" 
AR Path="/5F911003/5F9362B8" Ref="R211"  Part="1" 
F 0 "R211" H 7620 3746 50  0000 L CNN
F 1 "150" H 7620 3655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7480 3700 50  0001 C CNN
F 3 "~" H 7550 3700 50  0001 C CNN
	1    7550 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F9362BE
P 7900 3550
AR Path="/5F9362BE" Ref="C?"  Part="1" 
AR Path="/5F911003/5F9362BE" Ref="C210"  Part="1" 
F 0 "C210" V 7648 3550 50  0000 C CNN
F 1 "10u" V 7739 3550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7938 3400 50  0001 C CNN
F 3 "~" H 7900 3550 50  0001 C CNN
	1    7900 3550
	0    1    1    0   
$EndComp
Wire Wire Line
	7550 3550 7150 3550
Connection ~ 7150 3550
$Comp
L power:Earth #PWR?
U 1 1 5F9362C6
P 7550 3850
AR Path="/5F9362C6" Ref="#PWR?"  Part="1" 
AR Path="/5F911003/5F9362C6" Ref="#PWR0206"  Part="1" 
F 0 "#PWR0206" H 7550 3600 50  0001 C CNN
F 1 "Earth" H 7550 3700 50  0001 C CNN
F 2 "" H 7550 3850 50  0001 C CNN
F 3 "~" H 7550 3850 50  0001 C CNN
	1    7550 3850
	1    0    0    -1  
$EndComp
Connection ~ 7550 3850
Wire Wire Line
	7150 3850 7550 3850
Wire Wire Line
	7050 3550 7150 3550
Wire Wire Line
	7750 3550 7550 3550
Connection ~ 7550 3550
$Comp
L Diode:BAV99 D?
U 1 1 5F9362D3
P 8450 3200
AR Path="/5F9362D3" Ref="D?"  Part="1" 
AR Path="/5F911003/5F9362D3" Ref="D201"  Part="1" 
F 0 "D201" H 8450 3323 50  0000 C CNN
F 1 "BAV99" H 8450 3414 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8450 2700 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV99_SER.pdf" H 8450 3200 50  0001 C CNN
	1    8450 3200
	-1   0    0    1   
$EndComp
$Comp
L Diode:BAV99 D?
U 1 1 5F9362D9
P 8450 3750
AR Path="/5F9362D9" Ref="D?"  Part="1" 
AR Path="/5F911003/5F9362D9" Ref="D202"  Part="1" 
F 0 "D202" H 8450 3873 50  0000 C CNN
F 1 "BAV99" H 8450 3964 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8450 3250 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV99_SER.pdf" H 8450 3750 50  0001 C CNN
	1    8450 3750
	-1   0    0    1   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5F9362DF
P 8150 2800
AR Path="/5F9362DF" Ref="#PWR?"  Part="1" 
AR Path="/5F911003/5F9362DF" Ref="#PWR0207"  Part="1" 
F 0 "#PWR0207" H 8150 2650 50  0001 C CNN
F 1 "+3V3" H 8165 2973 50  0000 C CNN
F 2 "" H 8150 2800 50  0001 C CNN
F 3 "" H 8150 2800 50  0001 C CNN
	1    8150 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 3200 8150 2800
Wire Wire Line
	8150 3200 8150 3750
Connection ~ 8150 3200
Wire Wire Line
	8750 3200 8750 3750
Wire Wire Line
	8050 3550 8450 3550
Wire Wire Line
	8050 3000 8450 3000
$Comp
L power:Earth #PWR?
U 1 1 5F9362F1
P 8750 3850
AR Path="/5F9362F1" Ref="#PWR?"  Part="1" 
AR Path="/5F911003/5F9362F1" Ref="#PWR0208"  Part="1" 
F 0 "#PWR0208" H 8750 3600 50  0001 C CNN
F 1 "Earth" H 8750 3700 50  0001 C CNN
F 2 "" H 8750 3850 50  0001 C CNN
F 3 "~" H 8750 3850 50  0001 C CNN
	1    8750 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 3750 8750 3850
Connection ~ 8750 3750
$Comp
L power:Earth #PWR?
U 1 1 5F9362F9
P 9050 3850
AR Path="/5F9362F9" Ref="#PWR?"  Part="1" 
AR Path="/5F911003/5F9362F9" Ref="#PWR0209"  Part="1" 
F 0 "#PWR0209" H 9050 3600 50  0001 C CNN
F 1 "Earth" H 9050 3700 50  0001 C CNN
F 2 "" H 9050 3850 50  0001 C CNN
F 3 "~" H 9050 3850 50  0001 C CNN
	1    9050 3850
	1    0    0    -1  
$EndComp
Wire Notes Line
	6700 4050 8850 4050
Wire Notes Line
	8850 2500 6700 2500
Text Notes 6700 4250 0    50   ~ 0
Audio Filter\nhttps://learn.adafruit.com/introducing-the-raspberry-pi-zero/audio-outputs
Wire Notes Line
	8850 4050 8850 2500
Wire Notes Line
	6700 2500 6700 4050
Text GLabel 6650 3000 0    50   Input ~ 0
PWM0
Text GLabel 6650 3550 0    50   Input ~ 0
PWM1
Wire Wire Line
	6650 3000 6750 3000
Wire Wire Line
	6650 3550 6750 3550
Wire Wire Line
	1900 3500 1750 3500
Wire Wire Line
	1750 3500 1750 2600
Wire Wire Line
	4950 3400 4900 3400
Wire Wire Line
	4900 3500 5050 3500
Wire Wire Line
	5050 2600 1750 2600
Connection ~ 1750 2600
Wire Wire Line
	4900 3100 5200 3100
Wire Wire Line
	4900 3200 5650 3200
Wire Wire Line
	4900 3600 5650 3600
Wire Wire Line
	1350 3100 1900 3100
Wire Wire Line
	1900 3200 750  3200
Wire Wire Line
	1900 4300 750  4300
$Comp
L Device:R R201
U 1 1 5FB3ACC6
P 1600 3600
F 0 "R201" V 1550 3850 50  0000 C CNN
F 1 "0" V 1600 3600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1530 3600 50  0001 C CNN
F 3 "~" H 1600 3600 50  0001 C CNN
	1    1600 3600
	0    1    1    0   
$EndComp
$Comp
L Device:R R202
U 1 1 5FB3C6AB
P 1600 3700
F 0 "R202" V 1550 3950 50  0000 C CNN
F 1 "0" V 1600 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1530 3700 50  0001 C CNN
F 3 "~" H 1600 3700 50  0001 C CNN
	1    1600 3700
	0    1    1    0   
$EndComp
$Comp
L Device:R R203
U 1 1 5FB3D987
P 1600 3800
F 0 "R203" V 1550 4050 50  0000 C CNN
F 1 "0" V 1600 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1530 3800 50  0001 C CNN
F 3 "~" H 1600 3800 50  0001 C CNN
	1    1600 3800
	0    1    1    0   
$EndComp
$Comp
L Device:R R204
U 1 1 5FB3EABF
P 1600 3900
F 0 "R204" V 1550 4150 50  0000 C CNN
F 1 "0" V 1600 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1530 3900 50  0001 C CNN
F 3 "~" H 1600 3900 50  0001 C CNN
	1    1600 3900
	0    1    1    0   
$EndComp
$Comp
L Device:R R205
U 1 1 5FB3FFE7
P 1600 4000
F 0 "R205" V 1550 4250 50  0000 C CNN
F 1 "0" V 1600 4000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1530 4000 50  0001 C CNN
F 3 "~" H 1600 4000 50  0001 C CNN
	1    1600 4000
	0    1    1    0   
$EndComp
$Comp
L Device:R R206
U 1 1 5FB41149
P 1600 4100
F 0 "R206" V 1550 4350 50  0000 C CNN
F 1 "0" V 1600 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1530 4100 50  0001 C CNN
F 3 "~" H 1600 4100 50  0001 C CNN
	1    1600 4100
	0    1    1    0   
$EndComp
Connection ~ 1450 3600
Connection ~ 1450 3800
Connection ~ 1450 4000
Wire Wire Line
	1750 4000 1900 4000
$Comp
L Device:R R207
U 1 1 5FB56071
P 1600 4200
F 0 "R207" V 1550 4450 50  0000 C CNN
F 1 "0" V 1600 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1530 4200 50  0001 C CNN
F 3 "~" H 1600 4200 50  0001 C CNN
	1    1600 4200
	0    1    1    0   
$EndComp
Connection ~ 1450 4200
Wire Wire Line
	1450 4200 1450 4400
Wire Wire Line
	1750 4200 1900 4200
$Comp
L dk_Barrel-Audio-Connectors:SJ1-3523N CON201
U 1 1 5FB6EE5F
P 1350 2500
F 0 "CON201" H 1408 2887 60  0000 C CNN
F 1 "SJ1-3523N" H 1408 2781 60  0000 C CNN
F 2 "digikey-footprints:Headphone_Jack_3.5mm_SJ1-3523N" H 1550 2700 60  0001 L CNN
F 3 "https://www.cui.com/product/resource/digikeypdf/sj1-352xn_series.pdf" H 1550 2800 60  0001 L CNN
F 4 "CP1-3523N-ND" H 1550 2900 60  0001 L CNN "Digi-Key_PN"
F 5 "SJ1-3523N" H 1550 3000 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 1550 3100 60  0001 L CNN "Category"
F 7 "Barrel - Audio Connectors" H 1550 3200 60  0001 L CNN "Family"
F 8 "https://www.cui.com/product/resource/digikeypdf/sj1-352xn_series.pdf" H 1550 3300 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/cui-inc/SJ1-3523N/CP1-3523N-ND/738689" H 1550 3400 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN JACK STEREO 3.5MM R/A" H 1550 3500 60  0001 L CNN "Description"
F 11 "CUI Inc." H 1550 3600 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1550 3700 60  0001 L CNN "Status"
	1    1350 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 3500 5050 2600
Wire Wire Line
	1900 3400 1850 3400
Wire Wire Line
	1850 3400 1850 2500
Wire Wire Line
	1750 2400 4950 2400
Wire Wire Line
	4950 2400 4950 3400
Wire Wire Line
	1750 2500 1850 2500
$Comp
L dk_Barrel-Audio-Connectors:SJ1-3523N CON202
U 1 1 5FBA391C
P 9500 3100
F 0 "CON202" H 9172 3153 60  0000 R CNN
F 1 "SJ1-3523N" H 9172 3047 60  0000 R CNN
F 2 "digikey-footprints:Headphone_Jack_3.5mm_SJ1-3523N" H 9700 3300 60  0001 L CNN
F 3 "https://www.cui.com/product/resource/digikeypdf/sj1-352xn_series.pdf" H 9700 3400 60  0001 L CNN
F 4 "CP1-3523N-ND" H 9700 3500 60  0001 L CNN "Digi-Key_PN"
F 5 "SJ1-3523N" H 9700 3600 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 9700 3700 60  0001 L CNN "Category"
F 7 "Barrel - Audio Connectors" H 9700 3800 60  0001 L CNN "Family"
F 8 "https://www.cui.com/product/resource/digikeypdf/sj1-352xn_series.pdf" H 9700 3900 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/cui-inc/SJ1-3523N/CP1-3523N-ND/738689" H 9700 4000 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN JACK STEREO 3.5MM R/A" H 9700 4100 60  0001 L CNN "Description"
F 11 "CUI Inc." H 9700 4200 60  0001 L CNN "Manufacturer"
F 12 "Active" H 9700 4300 60  0001 L CNN "Status"
	1    9500 3100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8450 3000 9100 3000
Connection ~ 8450 3000
Wire Wire Line
	9100 3100 8950 3100
Wire Wire Line
	8950 3100 8950 3750
Wire Wire Line
	8950 3750 8750 3750
Wire Wire Line
	9050 3850 9050 3200
Wire Wire Line
	9050 3200 9100 3200
Wire Wire Line
	1450 3600 1450 3700
Wire Wire Line
	1450 3800 1450 3900
Wire Wire Line
	1450 4000 1450 4100
Connection ~ 1450 4100
Wire Wire Line
	1450 4100 1450 4200
Connection ~ 1450 3900
Wire Wire Line
	1450 3900 1450 4000
Connection ~ 1450 3700
Wire Wire Line
	1450 3700 1450 3800
Wire Wire Line
	1750 3700 1900 3700
Connection ~ 1450 4400
Wire Wire Line
	1450 4400 1900 4400
Wire Wire Line
	1900 3600 1750 3600
Wire Wire Line
	1900 3800 1750 3800
Wire Wire Line
	1900 3900 1750 3900
Wire Wire Line
	1900 4100 1750 4100
Wire Wire Line
	1350 4400 1450 4400
Wire Wire Line
	4900 3300 5200 3300
Wire Wire Line
	5650 3200 5650 3300
Wire Wire Line
	5500 3300 5650 3300
Connection ~ 5650 3300
Wire Wire Line
	5650 3300 5650 3600
$EndSCHEMATC
